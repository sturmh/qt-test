import random
from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__MainWindow import Ui_MainWindow
from ButtonListWidgetItem import ButtonListWidgetItem

buttons = ["VBAT", "VDD", "VAA", "VLED", "AD_DETECT", "LCD", "IG", "AO", "BARCODE"]

class MainWindow(QtGui.QMainWindow, Ui_MainWindow):    
    def __init__(self, app, parent=None):
        QtGui.QMainWindow.__init__(self, parent=parent)
        app.installEventFilter(self)

        self.setWindowFlags(QtCore.Qt.CustomizeWindowHint | QtCore.Qt.FramelessWindowHint)        
        self.setupUi(self)

        listWidget = self.buttons_listWidget        
        
        for button in buttons:            
            item = QtGui.QListWidgetItem(listWidget)            
            itemWidget = ButtonListWidgetItem(button)
            item.setSizeHint(itemWidget.minimumSize())
            
            itemWidget.setState(random.randint(0, 2))

            listWidget.addItem(item)
            listWidget.setItemWidget(item, itemWidget)

    def eventFilter(self, obj, e):                
        if e.type() == QtCore.QEvent.MouseButtonPress or e.type() == QtCore.QEvent.TouchBegin:
            self.randomizeButtonState()
            e.accept()
            return True            

        return QtCore.QObject.eventFilter(self, obj, e)

    def keyPressEvent(self, e):
        # need to do fancier stuff to get other keys, for now dont care
        if e.key() == QtCore.Qt.Key_Escape:
            e.accept()
            self.close()

    def randomizeButtonState(self):
        listWidget = self.buttons_listWidget
        for i in range(0, listWidget.count()):
            listWidget.itemWidget(listWidget.item(i)).setState(random.randint(0, 2)) 