from PyQt4 import QtGui
from PyQt4 import QtCore
from ui__ButtonListWidgetItem import Ui_button_listwidget_item

class ButtonListWidgetItem(QtGui.QWidget):
    INACTIVE = 0
    OFF = 1
    ON = 2

    def __init__(self, text, parent = None):
        super(ButtonListWidgetItem, self).__init__(parent)
        self.ui = Ui_button_listwidget_item()
        self.ui.setupUi(self)
        self.ui.label.setText(text)        
        self.setState(ButtonListWidgetItem.INACTIVE)

    def setState(self, state):
        self.state = state

        if state == ButtonListWidgetItem.INACTIVE:
            self.setStyleSheet("")
            self.ui.label.setStyleSheet('''color:rgb(200, 200, 200)''')
        elif state == ButtonListWidgetItem.OFF:
            self.setStyleSheet('''background-color:red''')
            self.ui.label.setStyleSheet('''color:black''')
        elif state == ButtonListWidgetItem.ON:
            self.ui.label.setStyleSheet('''color:black''')
            self.setStyleSheet('''background-color:green''')  