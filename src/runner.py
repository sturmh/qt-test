#/usr/bin/python

import sys
import signal
import random
from PyQt4 import QtGui
from PyQt4 import QtCore
from MainWindow import MainWindow

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL) # makes ctrl-c work
    app = QtGui.QApplication(sys.argv)
    w = MainWindow(app)        
    w.showFullScreen()
    sys.exit(app.exec_())
    