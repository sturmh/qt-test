#!/bin/sh

APPNAME="statusApp"
CLONE_DIR="temp"

./compile_ui.sh
cd src
pyinstaller -F runner.py

sudo mv dist/runner /usr/local/bin/$APPNAME

rm -rf dist build

cat > $APPNAME.desktop << EOL
[Desktop Entry]
Type=Application
Encoding=UTF-8
Version=1.0
Name=${APPNAME}
Name[C]=${APPNAME}
Exec=/usr/local/bin/${APPNAME}

EOL

sudo mv $APPNAME.desktop /usr/share/applications/

rm ~/Desktop/$APPNAME 2> /dev/null
sudo rm /etc/xdg/autostart/$APPNAME.desktop 2> /dev/null
sudo rm /usr/share/applications/$APPNAME.desktop 2> /dev/null

ln -s  /usr/share/applications/$APPNAME.desktop ~/Desktop/$APPNAME
sudo ln -s /usr/share/applications/$APPNAME.desktop /etc/xdg/autostart/$APPNAME.desktop

